package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@SpringBootApplication
public class SalvoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerRepository,GameRepository gameRepository, GamePlayerRepository gamePlayerRepository, ShipRepository shipRepository, SalvoRepository salvoRepository, ScoreRepository scoreRepository) {
		return (args) -> {

			//Guardo los jugadores

			Player p1 = new Player("jbauer@ctu.gov", passwordEncoder().encode("24"));
			playerRepository.save(p1);

			Player p2 = new Player("c.obrian@ctu.gov",passwordEncoder().encode("42"));
			playerRepository.save(p2);

			Player p3 = new Player("kim_bauer@gmail.com",passwordEncoder().encode("kb"));
			playerRepository.save(p3);

			Player p4 = new Player("t.almeida@ctu.gov", passwordEncoder().encode("mole"));
			playerRepository.save(p4);

			Game g1 = new Game();
			Date date1 = new Date();
			g1.setCreationDate(date1);

			Game g2 = new Game();
			Date date2 = Date.from(date1.toInstant().plusSeconds(3600));
			g2.setCreationDate(date2);

			Game g3 = new Game();
			Date date3 = Date.from(date2.toInstant().plusSeconds(3600));
			g3.setCreationDate(date3);

			Game g4 = new Game();
			Date date4 = Date.from(date3.toInstant().plusSeconds(3600));
			g4.setCreationDate(date4);

			Game g5 = new Game();
			Date date5 = Date.from(date4.toInstant().plusSeconds(3600));
			g5.setCreationDate(date5);

			Game g6 = new Game();
			Date date6 = Date.from(date5.toInstant().plusSeconds(3600));
			g6.setCreationDate(date6);

			Game g7 = new Game();
			Date date7 = Date.from(date6.toInstant().plusSeconds(3600));
			g7.setCreationDate(date7);

			Game g8 = new Game();
			Date date8 = Date.from(date7.toInstant().plusSeconds(3600));
			g8.setCreationDate(date8);

			gameRepository.save(g1);
			gameRepository.save(g2);
			gameRepository.save(g3);
			gameRepository.save(g4);
			gameRepository.save(g5);
			gameRepository.save(g6);
			gameRepository.save(g7);
			gameRepository.save(g8);

			//Creando GamePlayers

			// Game 1 (p1,p2)
			GamePlayer gp1 = new GamePlayer(g1,p1,date1);
			GamePlayer gp2 = new GamePlayer(g1,p2,date1);

			//Game 2 (p3,p1)
			GamePlayer gp3 = new GamePlayer(g2,p3,date2);
			GamePlayer gp4 = new GamePlayer(g2,p1,date2);

			//Game 3 (p2,p4)
			GamePlayer gp5 = new GamePlayer(g3,p2,date3);
			GamePlayer gp6 = new GamePlayer(g3,p4,date3);

			//Game 4 (p2,p1)
			GamePlayer gp7 = new GamePlayer(g4,p2,date4);
			GamePlayer gp8 = new GamePlayer(g4,p1,date4);

			// Game 5 (p4,p3)
			GamePlayer gp9 = new GamePlayer(g5,p4,date5);
			GamePlayer gp10 = new GamePlayer(g5,p3,date5);

			//Game 6 (p3)
			GamePlayer gp11 = new GamePlayer(g6,p3,date6);

			//Game 7 (p4)
			GamePlayer gp12 = new GamePlayer(g7,p4,date7);

			//Game 8 (p3,p4)
			GamePlayer gp13 = new GamePlayer(g8,p2,date8);
			GamePlayer gp14 = new GamePlayer(g8,p1,date8);


			gamePlayerRepository.save(gp1);
			gamePlayerRepository.save(gp2);
			gamePlayerRepository.save(gp3);
			gamePlayerRepository.save(gp4);
			gamePlayerRepository.save(gp5);
			gamePlayerRepository.save(gp6);
			gamePlayerRepository.save(gp7);
			gamePlayerRepository.save(gp8);
			gamePlayerRepository.save(gp9);
			gamePlayerRepository.save(gp10);
			gamePlayerRepository.save(gp11);
			gamePlayerRepository.save(gp12);
			gamePlayerRepository.save(gp13);
			gamePlayerRepository.save(gp14);

			//Creando los ships y us ubicaciones, por cada Game

			//Para game 1:

			List<String> shiplocation1 = new ArrayList<String>();
			shiplocation1.add("H2");
			shiplocation1.add("H3");
			shiplocation1.add("H4");

			List<String> shiplocation2 = new ArrayList<String>();
			shiplocation2.add("E1");
			shiplocation2.add("F1");
			shiplocation2.add("G1");

			List<String> shiplocation3 = new ArrayList<String>();
			shiplocation3.add("B4");
			shiplocation3.add("B5");

			List<String> shiplocation4 = new ArrayList<String>();
			shiplocation4.add("B5");
			shiplocation4.add("C5");
			shiplocation4.add("D5");

			List<String> shiplocation5 = new ArrayList<String>();
			shiplocation5.add("F1");
			shiplocation5.add("F2");

			//Para game 2:

			List<String> ship2location1 = new ArrayList<String>();
			ship2location1.add("B5");
			ship2location1.add("C5");
			ship2location1.add("D5");

			List<String> ship2location2 = new ArrayList<String>();
			ship2location2.add("C6");
			ship2location2.add("C7");

			List<String> ship2location3 = new ArrayList<String>();
			ship2location3.add("A2");
			ship2location3.add("A3");
			ship2location3.add("A4");

			List<String> ship2location4 = new ArrayList<String>();
			ship2location4.add("G6");
			ship2location4.add("H6");

//Game 1

			Ship ship1 = new Ship("Destroyer",gp1,shiplocation1);
			Ship ship2 = new Ship("Submarine",gp1,shiplocation2);
			Ship ship3 = new Ship("Patrol",gp1,shiplocation3);
			Ship ship4 = new Ship("Destroyer",gp2,shiplocation4);
			Ship ship5 = new Ship("Patrol",gp2,shiplocation5);

//Game 2

			Ship ship6 = new Ship("Destroyer",gp3,ship2location1);
			Ship ship7 = new Ship("Patrol",gp3,ship2location2);
			Ship ship8 = new Ship("Submarine",gp4,ship2location3);
			Ship ship9 = new Ship("Patrol",gp4,ship2location4);

//Repositorios

			shipRepository.save(ship1);
			shipRepository.save(ship2);
			shipRepository.save(ship3);
			shipRepository.save(ship4);
			shipRepository.save(ship5);
			shipRepository.save(ship6);
			shipRepository.save(ship7);
			shipRepository.save(ship8);
			shipRepository.save(ship9);

			//Creando los Salvos

//Para game 1 / player 1 / turno 1

			List<String> salvo1location = new ArrayList<String>();
			salvo1location.add("B5");
			salvo1location.add("C5");
			salvo1location.add("F1");

			Salvo salvo1 = new Salvo(gp1,1,salvo1location);

//Para game 1 / player 2 / turno 1

			List<String> salvo2location = new ArrayList<String>();
			salvo2location.add("B4");
			salvo2location.add("B5");
			salvo2location.add("B6");

			Salvo salvo2 = new Salvo(gp2,1,salvo2location);

//Para game 1 / player 1 / turno 2

			List<String> salvo3location = new ArrayList<String>();
			salvo3location.add("F2");
			salvo3location.add("D5");

			Salvo salvo3 = new Salvo(gp1,2,salvo3location);

//Para game 1 / player 2 / turno 2

			List<String> salvo4location = new ArrayList<String>();
			salvo4location.add("E1");
			salvo4location.add("H2");
			salvo4location.add("A2");

			Salvo salvo4 = new Salvo(gp2,2,salvo4location);

//Para game 2 / player 1 / turno 1

			List<String> salvo5location = new ArrayList<String>();
			salvo5location.add("A2");
			salvo5location.add("A4");
			salvo5location.add("G6");

			Salvo salvo5 = new Salvo(gp3,1,salvo5location);

//Para game 2 / player 2 / turno 1

			List<String> salvo6location = new ArrayList<String>();
			salvo6location.add("B5");
			salvo6location.add("C5");
			salvo6location.add("D7");

			Salvo salvo6 = new Salvo(gp4,1,salvo6location);

//Para game 2 / player 1 / turno 2

			List<String> salvo7location = new ArrayList<String>();
			salvo7location.add("A3");
			salvo7location.add("H6");

			Salvo salvo7 = new Salvo(gp3,2,salvo7location);

//Para game 2 / player 2 / turno 2

			List<String> salvo8location = new ArrayList<String>();
			salvo8location.add("C5");
			salvo8location.add("C6");

			Salvo salvo8 = new Salvo(gp4,2,salvo8location);

//Repositorios

			salvoRepository.save(salvo1);
			salvoRepository.save(salvo2);
			salvoRepository.save(salvo3);
			salvoRepository.save(salvo4);
			salvoRepository.save(salvo5);
			salvoRepository.save(salvo6);
			salvoRepository.save(salvo7);
			salvoRepository.save(salvo8);

			//Agregando los Scores de los juegos

			//Testing note: finishDate: 30 minutos luego de creationDate

			Date fDate1 = Date.from(date1.toInstant().plusSeconds(1800));
			Date fDate2 = Date.from(date2.toInstant().plusSeconds(1800));
			Date fDate3 = Date.from(date3.toInstant().plusSeconds(1800));
			Date fDate4 = Date.from(date4.toInstant().plusSeconds(1800));
			Date fDate5 = Date.from(date5.toInstant().plusSeconds(1800));
			Date fDate6 = Date.from(date6.toInstant().plusSeconds(1800));


			Score score1 = new Score(fDate1,g1,p1,1);
			Score score2 = new Score(fDate1,g1,p2,0);
			Score score3 = new Score(fDate2,g2,p3,0.5);
			Score score4 = new Score(fDate2,g2,p4,0.5);
			Score score5 = new Score(fDate3,g3,p2,1);
			Score score6 = new Score(fDate3,g3,p4,0);
			Score score7 = new Score(fDate3,g4,p2,0.5);
			Score score8 = new Score(fDate3,g4,p1,0.5);

			scoreRepository.save(score1);
			scoreRepository.save(score2);
			scoreRepository.save(score3);
			scoreRepository.save(score4);
			scoreRepository.save(score5);
			scoreRepository.save(score6);
			scoreRepository.save(score7);
			scoreRepository.save(score8);

		};
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}

//Subclase para configurar seguridad

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	PlayerRepository playerRepository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(inputName-> {
			Player player = playerRepository.findByUserName(inputName);
			if (player != null) {
				return new User(player.getUserName(), player.getPassword(),
						AuthorityUtils.createAuthorityList("USER"));
			} else {
				throw new UsernameNotFoundException("Unknown user: " + inputName);
			}
		});
	}
}

//Otra subclase para autorizar quien puede ver que
@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/web/games_3.html").permitAll()
				.antMatchers("/web/games.html").permitAll()
				.antMatchers("/web/**").permitAll()
                .antMatchers("/api/games").permitAll()
				.antMatchers("/api/game").permitAll()
                .antMatchers("/api/players").permitAll()
                .antMatchers("/api/games/players/**").permitAll()
		        .antMatchers("/api/game_view/**").permitAll()//hasAuthority("USER")
                .antMatchers("/rest/*").denyAll()
                .anyRequest().permitAll();

                http.formLogin()
                        .usernameParameter("username")
                        .passwordParameter("password")
                        .loginPage("/api/login");

                http.logout().logoutUrl("/api/logout");



		// turn off checking for CSRF tokens
		http.csrf().disable();

		// if user is not authenticated, just send an authentication failure response
		http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if login is successful, just clear the flags asking for authentication
		http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

		// if login fails, just send an authentication failure response
		http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if logout is successful, just send a success response
		http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
	}

	private void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		}
	}



}

