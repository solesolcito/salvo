package com.codeoftheweb.salvo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController  //hacer un rest controller va a hacer que la salida sea un json
//This annotation was introduced in Spring 4.0 to simplify the creation of RESTful web services. It’s a convenience
// annotation that combines @Controller and @ResponseBody – which eliminates the need to annotate every request handling
// method of the controller class with the @ResponseBody annotation
@RequestMapping("/api")//el controlador tiene una ruta de acceso
//Es una anotación de Spring.This annotation maps HTTP requests to handler methods of MVC and REST controllers.
// Annotation for mapping web requests onto methods in request-handling classes with flexible method signatures.
public class SalvoController {

    //get game repository, all repositories needed
    @Autowired//This annotation allows Spring to resolve and inject collaborating beans into your bean.
    private GameRepository gameRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private ScoreRepository scoreRepository;

    //MAPEOS

    //Para poder ver el leaderboard cuando se lo llama en games.html
    @RequestMapping("/leaderBoard")
    public List<Object> getLeaderBoard() {
        return playerRepository.findAll().stream().map(player -> leaderListDTO(player)).collect(Collectors.toList());
    }

    @RequestMapping("/games")//este es del task1modulo2
    public Map<String, Object> makeLoggedPlayer(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken)
            dto.put("player", "Guest");
        else
            dto.put("player", loggedPlayerDTO(playerRepository.findByUserName(authentication.getName())));
        dto.put("games", getGames());

        return dto;
    }

    @RequestMapping(path = "/games", method = RequestMethod.POST)//task2 modulo2
    public ResponseEntity<Map<String, Object>> createGame(Authentication authentication) {
        if (authentication.getName().isEmpty()) {
            return new ResponseEntity<>(makeMap("Error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

        Game newGame = gameRepository.save(new Game(new Date()));
        GamePlayer newGamePlayer = gamePlayerRepository.save(new GamePlayer(newGame, playerRepository.findByUserName(authentication.getName()), new Date()));


        return new ResponseEntity<>(makeMap("gpid", newGamePlayer.getGamePlayerId()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/game/{nn}/players", method = RequestMethod.POST)//POST
    //The annotation is capable of handling HTTP request methods, such as GET, PUT, POST, DELETE, and PATCH.
    //By default all requests are assumed to be of HTTP GET type.
    public ResponseEntity<Object> joinGame(@PathVariable Long nn, Authentication authentication) {//
        Map<String, Object> response = new LinkedHashMap<String, Object>();
        if (getLoggedPlayer(authentication) == null) {
            response.put("error", "no autenticado");
            return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
        } else {
            Game game = gameRepository.getOne(nn);
            if (game != null) {
                if (game.getGamePlayers().size() < 2) {
                    Player player = playerRepository.findByUserName(authentication.getName());
                    GamePlayer gamePlayer = gamePlayerRepository.save(new GamePlayer(game, player, game.getCreationDate()));
                    response.put("gpid", gamePlayer.getGamePlayerId());
                    return new ResponseEntity<>(response, HttpStatus.CREATED);
                } else {
                    response.put("error", "game is full");
                    //return new ResponseEntity<>(response,HttpStatus.FORBIDDEN);
                    return new ResponseEntity<>(makeMap("error", "This Game Is Full" + game.getGamePlayers().stream().map(gamePlayer -> gamePlayer.getPlayer().getUserName()).collect(Collectors.toList())), HttpStatus.FORBIDDEN);
                }
            } else {
                response.put("error", "no such game");
                return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
            }
        }
    }

    @RequestMapping(path = "/players", method = RequestMethod.POST)//task2 modulo2
    public ResponseEntity<Map<String, Object>> createPlayer(
            @RequestParam String userName, @RequestParam String password) {//The @RequestParam annotation is used with
        // @RequestMapping to bind a web request parameter to the parameter of the handler method.  The value specifies
        // the request param that needs to be mapped to the handler method parameter

        if (userName.isEmpty()) {
            return new ResponseEntity<>(makeMap("error", "No name given"), HttpStatus.FORBIDDEN);
        }

        Player player = playerRepository.findByUserName(userName);
        if (player != null) {
            return new ResponseEntity<>(makeMap("error", "Name in use"), HttpStatus.CONFLICT);
        }

        Player newPlayer = playerRepository.save(new Player(userName, passwordEncoder.encode(password)));

        return new ResponseEntity<>(makeMap("id", newPlayer.getPlayerId()), HttpStatus.CREATED);
    }

    @RequestMapping("/game_view/{gpid}")
    public ResponseEntity<Object> cheat(@PathVariable long gpid, Authentication authentication) {
        if (authentication.getName()==null || authentication instanceof AnonymousAuthenticationToken){
                return new ResponseEntity<>(makeMap("Error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        Player player = getLoggedPlayer(authentication);
        GamePlayer gamePlayer = gamePlayerRepository.findById(gpid).orElse(null);
        //if the gameplayer do not exist, return error
        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "Forbidden"), HttpStatus.FORBIDDEN);
        }

        GamePlayer oppGamePlayer = getOpponentGamePlayer(gamePlayer);

        //if game doesn't have an opponent, there's no oppGamePlayer
        if (oppGamePlayer == null) {
            return new ResponseEntity<>(makeMap("error, game player doesn't exist", "Forbidden"), HttpStatus.FORBIDDEN);
        }

        if (player.getPlayerId() != gamePlayer.getPlayer().getPlayerId()) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getGame().getGameId());
        dto.put("created", gamePlayer.getGame().getCreationDate().getTime());
        dto.put("gamePlayers", getGamePlayersList(gamePlayer.getGame().getGamePlayers()));//"gamePlayer"
        dto.put("ships", getShipList(gamePlayer.getShips()));
        dto.put("salvoes", getSalvoList(gamePlayer));        //return dto;
        dto.put("hits", makeHitsDTO(gamePlayer, oppGamePlayer));
        dto.put("gameState",getGameState(gamePlayer));//hace que se vea el radar y los barquitos
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(path = "/games/players/{gamePlayer_id}/ships")
    public ResponseEntity<Map<String, Object>> addShips(@PathVariable long gamePlayer_id, @RequestBody Set<Ship> ships, Authentication authentication) {

        if (getLoggedPlayer(authentication) == null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Player player = playerRepository.findByUserName(authentication.getName());//getCurrentPlayerByName(authentication.getName());

        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayer_id).orElse(null);

        if (gamePlayer == null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        if (player.getPlayerId() != gamePlayer.getPlayer().getPlayerId())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        if (gamePlayer.getShips().size() >= 5) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        for (Ship ship : ships) {
            ship.setGamePlayer(gamePlayer);
            shipRepository.save(ship);
        }

        return new ResponseEntity<>(makeMap("OK", "Ships placed!"), HttpStatus.OK);
    }

   @RequestMapping(path = "/games/players/{gamePlayerId}/salvoes")
   public ResponseEntity<Map<String, Object>> addSalvo(@PathVariable long gamePlayerId, @RequestBody Salvo salvo, Authentication authentication){
       if(isGuest(authentication)) {
           return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
       }

       Player player = playerRepository.findByUserName(authentication.getName());
       GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).orElse(null);

       if(gamePlayer == null) {
           return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
       }

       if(player.getPlayerId() != gamePlayer.getPlayer().getPlayerId()){
           return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
       }

       salvo.setTurnNumber(getCurrentTurn(gamePlayer, getOpponentGamePlayer(gamePlayer)));

       if(existSalvo(salvo, gamePlayer.getSalvoes())){
           return new ResponseEntity<>(makeMap("error", "You have already post your salvo!"), HttpStatus.FORBIDDEN);
       }

       salvo.setGamePlayer(gamePlayer);
       salvoRepository.save(salvo);

       return new ResponseEntity<>(makeMap("OK", "Salvo placed!"), HttpStatus.CREATED);
   }

    //metodos y cosas

    Player getLoggedPlayer(Authentication authentication) {
        return playerRepository.findByUserName(authentication.getName());
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    private Map<String, Object> GameDTO(Game game) {//el mapa es un diccionario de datos y por cada valor le tengo q ASIGNAR UNA CLAVE
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", game.getGameId());
        dto.put("creationDate", game.getCreationDate());
        dto.put("gamePlayers", getGamePlayersList(game.getGamePlayers()));//si saco esta linea se ve el json como en el pto 5 del task 2
        dto.put("scores", getScores(game.getGamePlayers()));

        return dto;
    }

    public Map<String, Object> makeScoreDTO(GamePlayer gamePlayer) {//POR QUÉ ES PUBLICO??
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerID", gamePlayer.getPlayer().getPlayerId());
        if (gamePlayer.getScore() != null)
            dto.put("score", gamePlayer.getScore().getScore());

        return dto;
    }

    private Map<String, Object> playerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getPlayerId());
        dto.put("username", player.getUserName());
        dto.put("score", playerScoreToDTO(player));

        return dto;
    }

    private Map<String, Object> gamePlayersDTO(GamePlayer gamePlayer) {
        Map<String, Object> d = new LinkedHashMap<String, Object>();
        d.put("id", gamePlayer.getGamePlayerId());//gpid
        d.put("player", playerDTO(gamePlayer.getPlayer()));

        return d;
    }

    private Map<String, Object> shipDTO(Ship ship) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("type", ship.getShipType());
        dto.put("locations", ship.getLocations());

        return dto;
    }

    private Map<String, Object> salvoDTO(Salvo salvo) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("turn", salvo.getTurnNumber());
        dto.put("player", salvo.getGamePlayer().getPlayer().getPlayerId());
        dto.put("locations", salvo.getLocations());
        //dto.put("currentTurn",getCurrentTurn(salvo.getGamePlayer(),getOpponentGamePlayer(salvo.getGamePlayer())));

        return dto;
    }

    private Map<String, Object> leaderListDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("name", player.getUserName());
        dto.put("score", playerScoreToDTO(player));
        return dto;
    }

    private Map<String, Object> playerScoreToDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("total", (player.getWins() + player.getDraws() * 0.5));
        dto.put("won", (player.getWins()));
        dto.put("lost", (player.getLoses()));
        dto.put("tied", (player.getDraws()));
        return dto;
    }

    public Map<String, Object> loggedPlayerDTO(Player player) {//POR QUE ES PUBLICO?
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getPlayerId());
        dto.put("name", player.getUserName());

        return dto;
    }

    private Map<String, Object> makeHitsDTO(GamePlayer selfGP, GamePlayer opponentGP){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(selfGP, opponentGP));
        dto.put("opponent", getHits(opponentGP, selfGP));

        return dto;
    }

    public List<Map<String, Object>> getGames() {//metodo que devuelve los juegos, hago una lista de mapas
        return gameRepository
                .findAll()
                .stream()
                //stream maneja eficientemente nuestra coleccion de datos que esta trayendo el metodo
                .map(game -> GameDTO(game))
                .collect(Collectors.toList());
    }

    public List<Map<String, Object>> getScores(List<GamePlayer> gamePlayers) {
        return gamePlayers
                .stream()
                .map(gamePlayer -> makeScoreDTO(gamePlayer))
                .collect(Collectors.toList());
    }

    private List<Object> getGamePlayersList(List<GamePlayer> gamePlayers) {
        return gamePlayers.stream().map(gamePlayer -> gamePlayersDTO(gamePlayer))
                .collect(Collectors.toList());
    }

    private List<Map<String, Object>> getShipList(Set<Ship> ships) {
        return ships
                .stream()
                .map(ship -> shipDTO(ship))
                .collect(Collectors.toList());
    }

    private List<Map<String, Object>> getSalvoList(GamePlayer gamePlayer) {
        List<Map<String, Object>> salvoList = new ArrayList<>();
        gamePlayer.getGame().getGamePlayers().forEach(gp -> gp.getSalvoes().forEach(salvo -> salvoList.add(salvoDTO(salvo))));
        return salvoList;
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(key, value);
        return map;
    }

    private GamePlayer getOpponentGamePlayer(GamePlayer gamePlayer){
        List<GamePlayer> gamePlayers = gamePlayer.getGame().getGamePlayers();

        for(GamePlayer gp : gamePlayers){
            if(gp.getGamePlayerId() != gamePlayer.getGamePlayerId())
                return gp;
        }

        return new GamePlayer();
    }

    private List<Map> getHits(GamePlayer gamePlayer, GamePlayer oppGameplayer) {
        List<Map> hits = new ArrayList<>();

        Integer carrierDamage = 0;
        Integer battleshipDamage = 0;
        Integer submarineDamage = 0;
        Integer destroyerDamage = 0;
        Integer patrolboatDamage = 0;

        List <String> carrierLocation = new ArrayList<>();
        List <String> battleshipLocation = new ArrayList<>();
        List <String> submarineLocation = new ArrayList<>();
        List <String> destroyerLocation = new ArrayList<>();
        List <String> patrolboatLocation = new ArrayList<>();

        gamePlayer.getShips().forEach(ship -> {
            switch (ship.getShipType()) {
                case "carrier":
                    carrierLocation.addAll(ship.getLocations());
                    break;
                case "battleship":
                    battleshipLocation.addAll(ship.getLocations());
                    break;
                case "submarine":
                    submarineLocation.addAll(ship.getLocations());
                    break;
                case "destroyer":
                    destroyerLocation.addAll(ship.getLocations());
                    break;
                case "patrolboat":
                    patrolboatLocation.addAll(ship.getLocations());
                    break;
            }
        });

        for (Salvo salvo : oppGameplayer.getSalvoes()) {
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0;
            Integer missedShots = salvo.getLocations().size();

            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();

            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();
            salvoLocationsList.addAll(salvo.getLocations());
            for (String salvoShot : salvoLocationsList) {
                if (carrierLocation.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (battleshipLocation.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (submarineLocation.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (destroyerLocation.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (patrolboatLocation.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
            }

            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("carrier", carrierDamage);
            damagesPerTurn.put("battleship", battleshipDamage);
            damagesPerTurn.put("submarine", submarineDamage);
            damagesPerTurn.put("destroyer", destroyerDamage);
            damagesPerTurn.put("patrolboat", patrolboatDamage);

            hitsMapPerTurn.put("turn", salvo.getTurnNumber());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            hits.add(hitsMapPerTurn);
        }
        return hits;
    }

    private String getGameState(GamePlayer selfGP) {
        Set<Ship> selfShips = selfGP.getShips();
        Set<Salvo> selfSalvoes = selfGP.getSalvoes();
        if (selfShips.size() == 0){
            return "PLACESHIPS";
        }
        if (getOpponentGamePlayer(selfGP).getShips() == null){
            return "WAITINGFOROPP";
        }
        int turn = getCurrentTurn(selfGP, getOpponentGamePlayer(selfGP));
        Set<Ship> opponentShips = getOpponentGamePlayer(selfGP).getShips();
        Set<Salvo> opponentSalvoes = getOpponentGamePlayer(selfGP).getSalvoes();
        if (opponentShips.size() == 0){
            return "WAIT";
        }
        if(selfSalvoes.size() == opponentSalvoes.size()){
            Player self = selfGP.getPlayer();
            Game game = selfGP.getGame();
            if (allPlayerShipsSunk(selfShips, opponentSalvoes) && allPlayerShipsSunk(opponentShips, selfSalvoes)){
                Score score = new Score(new Date(), game,self,0.5f);
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "TIE";
            }
            if (allPlayerShipsSunk(selfShips, opponentSalvoes)){
                Score score = new Score(new Date(), game,self,0);
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "LOST";
            }
            if(allPlayerShipsSunk(opponentShips, selfSalvoes)){
                Score score = new Score(new Date(), game,self,1);
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "WON";
            }
        }
        if (selfSalvoes.size() != turn){
            return "PLAY";
        }
        return "WAIT";
    }

    private int getCurrentTurn(GamePlayer selfGP, GamePlayer opponentGP){
        int selfGPSalvoes = selfGP.getSalvoes().size();
        int opponentGPSalvoes = opponentGP.getSalvoes().size();

        int totalSalvoes = selfGPSalvoes + opponentGPSalvoes;

        if(totalSalvoes % 2 == 0)
            return totalSalvoes / 2 + 1;

        return (int) (totalSalvoes / 2.0 + 0.5);
    }

    private boolean allPlayerShipsSunk(Set<Ship> selfShips, Set<Salvo> opponentSalvoes){
        List<String> carrierLocations = new ArrayList<>();
        List<String> battleshipLocations = new ArrayList<>();
        List<String> patrolboatLocations = new ArrayList<>();
        List<String> submarineLocations = new ArrayList<>();
        List<String> destroyerLocations = new ArrayList<>();
        int carrierDamage = 0;
        int battleshipDamage = 0;
        int patrolboatDamage = 0;
        int submarineDamage = 0;
        int destroyerDamage = 0;
        for(Ship ship : selfShips){
            if(ship.getShipType().equals("carrier")){
                carrierLocations.addAll(ship.getLocations());
            }else if(ship.getShipType().equals("battleship")){
                battleshipLocations.addAll(ship.getLocations());
            }else if(ship.getShipType().equals("patrolboat")){
                patrolboatLocations.addAll(ship.getLocations());
            }else if(ship.getShipType().equals("submarine")){
                submarineLocations.addAll(ship.getLocations());
            }else if(ship.getShipType().equals("destroyer")){
                destroyerLocations.addAll(ship.getLocations());
            }
        }
        List<String> salvoesLocations = new ArrayList<>();
        for(Salvo salvo : opponentSalvoes){
            salvoesLocations.addAll(salvo.getLocations());
        }
        for(String salvoLocation : salvoesLocations){
            if(existLocation(salvoLocation, carrierLocations)){
                carrierDamage++;
            }else if(existLocation(salvoLocation, battleshipLocations)) {
                battleshipDamage++;
            }else if(existLocation(salvoLocation, patrolboatLocations)){
                patrolboatDamage++;
            }else if(existLocation(salvoLocation, submarineLocations)){
                submarineDamage++;
            }else if(existLocation(salvoLocation, destroyerLocations)){
                destroyerDamage++;
            }
        }
        if(carrierDamage == 5 && battleshipDamage == 4 && destroyerDamage == 3 && submarineDamage == 3 && patrolboatDamage == 2){
            return true;
        }
        return false;
    }

    private boolean existLocation(String location, List<String> locations){
        for(String _location : locations){
            if(location.equals(_location)){
                return true;
            }
        }
        return false;
    }
    private boolean existSalvo(Salvo salvo, Set<Salvo> salvoes){
        for(Salvo s : salvoes){
            if(salvo.getTurnNumber() == s.getTurnNumber()){
                return true;
            }
        }
        return false;
    }
    private boolean existScore(Score score, Game game){
        Set<Score> scores = game.getScores();
        for(Score s : scores){
            if(score.getPlayer().getUserName().equals(s.getPlayer().getUserName())){
                return true;
            }
        }
        return false;
    }

}





