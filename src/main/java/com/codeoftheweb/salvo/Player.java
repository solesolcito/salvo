package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long playerId;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private List<GamePlayer> gamePlayers;//crea una lista

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<Score> scores;// = new HashSet<Score>();//hash es diccionario


    private String userName;
    private String password;

    public Player() {}

    public Player(String userName, String password) {
        this.setUserName(userName);
        this.setPassword(password);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String toString() {
        return getUserName();
    }

    public long getPlayerId() {
        return playerId;
    }

    @JsonIgnore
    public List<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }

    public void setGamePlayers(List<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }

    //@JsonIgnore
    public void addGamePlayer(GamePlayer gamePlayer) {
        gamePlayer.setPlayer(this);
        getGamePlayers().add(gamePlayer);
    }

    public double getWins(){
        return getScores()
                .stream()
                .filter(score -> score.getScore()==1)
                .count();
    }

    public double getDraws(){
        return getScores()
                .stream()
                .filter(score -> score.getScore()==0.5)
                .count();
    }

    public double getLoses(){
        return getScores()
                .stream()
                .filter(score -> score.getScore()==0)
                .count();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public Score getScore(Game game){
        return game.getScores().stream().filter(score -> score.getPlayer().equals(this)).findFirst().orElse(null);
    }

    public Set<Score> getScores() {
            return scores;
        }

        public void setScores(Set<Score> scores) {
            this.scores = scores;
    }
}
