package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity

public class GamePlayer {//hola esto es un comentario de prueba

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long gamePlayerId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_id")
    private Player player;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="game_id")
    private Game game;

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    private Set<Ship> ships;//crea una lista

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    private Set<Salvo> salvoes;//crea una lista


    private Date joinDate;

    public GamePlayer(){
        this.ships = new HashSet<>();
        this.salvoes = new HashSet<>();
    }

    public GamePlayer (Game game,Player player,Date joinDate){
        this.game = game;
        this.setPlayer(player);
        this.setJoinDate(new Date());
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public long getGamePlayerId() {
        return gamePlayerId;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Set<Ship> getShips() {
        return ships;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }

    public Set<Salvo> getSalvoes() {
        return salvoes;
    }

    public void setSalvoes(Set<Salvo> salvoes) {
        this.salvoes = salvoes;
    }

    public Score getScore(){return getPlayer().getScore(getGame());}

    public boolean isTurnLoaded(int turn){
        Salvo salvoAux = this.getSalvoes().stream().filter(salvo -> salvo.getTurnNumber()==turn).findFirst().orElse(null);
        if (salvoAux==null){
            return false;
        }else{
            return true;
        }
    }
}
